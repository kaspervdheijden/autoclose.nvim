## Autoclose nvim

Auto closes unused buffer after a certain amount of time. Entering insert mode or modifying
the buffer disables autoclosing of a buffer.

### Installation

#### lazy.nvim
```lua
-- lua/plugins/autocmd-nvim.lua
return {
    'https://gitlab.com/kaspervdheijden/autoclose-nvim.git',
    event = 'VeryLazy',
    opts  = {
        autoclose = true, -- autoclose buffers, if you disable this you can manually call `require('autoclose-nvim').autoCloseBuffers()`
        delay     = 150,  -- delay after which buffers will be deleted after switching buffers
        timeout   = 60,   -- timeout after which buffers will be closed
    },
}
```
