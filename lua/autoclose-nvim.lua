local config, M = {}, {}

M.run = function ()
    local cur  = vim.api.nvim_get_current_buf()
    local now  = vim.fn.localtime()
    local bufs = {}

    for _, buf in ipairs(vim.api.nvim_list_bufs()) do
        if buf ~= cur and (vim.b[buf].closetime or 0) ~= 0 then
            if vim.bo[buf].buflisted and vim.api.nvim_buf_is_valid(buf) then
                if now >= vim.b[buf].closetime then
                    bufs[#bufs + 1] = buf
                end
            end
        end
    end

    local bufs_left = #bufs
    for _, buf in pairs(bufs) do
        if bufs_left < 2 then
            return
        end

        bufs_left = bufs_left - 1
        vim.cmd.bd(buf)
    end
end

M.disable = function () config.enabled = false end
M.enable = function () config.enabled = true end

M.setup = function (opts)
    local group = vim.api.nvim_create_augroup('autoclose-autocmd', { clear = true })

    config         = opts or {}
    config.enabled = config.enabled == nil or config.enabled

    vim.api.nvim_create_autocmd('BufEnter', {
        group    = group,
        callback = function ()
            local buf = vim.api.nvim_get_current_buf()
            if not vim.bo[buf].buflisted or not vim.api.nvim_buf_is_valid(buf) then
                return
            end

            if vim.b[buf].closetime ~= 0 then
                vim.b[buf].closetime = vim.fn.localtime() + (config.timeout or 60)
            end

            if config.enabled then
                vim.schedule(M.run, config.delay or 150)
            end
        end,
    })

    vim.api.nvim_create_autocmd({ 'InsertEnter', 'BufModifiedSet' }, {
        group    = group,
        callback = function ()
            vim.b[vim.api.nvim_get_current_buf()].closetime = 0
        end,
    })

    vim.api.nvim_create_user_command('AutoCloseDisable', M.disable, { desc = 'AutoClose disable', bang = true })
    vim.api.nvim_create_user_command('AutoCloseEnable',  M.enable,  { desc = 'AutoClose enable',  bang = true })
    vim.api.nvim_create_user_command('AutoCloseRun',     M.run,     { desc = 'AutoClose run',     bang = true })
end

return M
